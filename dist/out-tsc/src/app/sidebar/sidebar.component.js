"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
// Menu Items
// export const ROUTES: RouteInfo[] = [{
//         path: '/dashboard',
//         title: 'Dashboard',
//         type: 'link',
//         icontype: 'nc-icon nc-bank'
//     },{
//         path: '/components',
//         title: 'Components',
//         type: 'sub',
//         collapse: 'components',
//         icontype: 'nc-icon nc-layout-11',
//         children: [
//             {path: 'buttons', title: 'Buttons', ab:'B'},
//             {path: 'grid', title: 'Grid System', ab:'GS'},
//             {path: 'panels', title: 'Panels', ab:'P'},
//             {path: 'sweet-alert', title: 'Sweet Alert', ab:'SA'},
//             {path: 'notifications', title: 'Notifications', ab:'N'},
//             {path: 'icons', title: 'Icons', ab:'I'},
//             {path: 'typography', title: 'Typography', ab:'T'}
//         ]
//     },{
//         path: '/forms',
//         title: 'Forms',
//         type: 'sub',
//         collapse: 'forms',
//         icontype: 'nc-icon nc-ruler-pencil',
//         children: [
//             {path: 'regular', title: 'Regular Forms', ab:'RF'},
//             {path: 'extended', title: 'Extended Forms', ab:'EF'},
//             {path: 'validation', title: 'Validation Forms', ab:'VF'},
//             {path: 'wizard', title: 'Wizard', ab:'W'}
//         ]
//     },{
//         path: '/tables',
//         title: 'Tables',
//         type: 'sub',
//         collapse: 'tables',
//         icontype: 'nc-icon nc-single-copy-04',
//         children: [
//             {path: 'regular', title: 'Regular Tables', ab:'RT'},
//             {path: 'extended', title: 'Extended Tables', ab:'ET'},
//             {path: 'datatables.net', title: 'Datatables.net', ab:'DT'}
//         ]
//     },{
//         path: '/maps',
//         title: 'Maps',
//         type: 'sub',
//         collapse: 'maps',
//         icontype: 'nc-icon nc-pin-3',
//         children: [
//             {path: 'google', title: 'Google Maps', ab:'GM'},
//             {path: 'fullscreen', title: 'Full Screen Map', ab:'FSM'},
//             {path: 'vector', title: 'Vector Map', ab:'VM'}
//         ]
//     },{
//         path: '/widgets',
//         title: 'Widgets',
//         type: 'link',
//         icontype: 'nc-icon nc-box'
//     },{
//         path: '/charts',
//         title: 'Charts',
//         type: 'link',
//         icontype: 'nc-icon nc-chart-bar-32'
//     },{
//         path: '/calendar',
//         title: 'Calendar',
//         type: 'link',
//         icontype: 'nc-icon nc-calendar-60'
//     },{
//         path: '/pages',
//         title: 'Pages',
//         collapse: 'pages',
//         type: 'sub',
//         icontype: 'nc-icon nc-book-bookmark',
//         children: [
//             {path: 'timeline', title: 'Timeline Page', ab:'T'},
//             {path: 'user', title: 'User Page', ab:'UP'},
//             {path: 'login', title: 'Login Page', ab:'LP'},
//             {path: 'register', title: 'Register Page', ab:'RP'},
//             {path: 'lock', title: 'Lock Screen Page', ab:'LSP'}
//         ]
//     }
// ];
exports.ROUTES = [{
        path: '/c/dashboard',
        title: 'Dashboard',
        type: 'link',
        icontype: 'nc-icon nc-atom'
    }, {
        path: '/c/car-registration',
        title: 'Car registration',
        type: 'link',
        icontype: 'nc-icon nc-delivery-fast'
    }
];
var SidebarComponent = /** @class */ (function () {
    function SidebarComponent() {
    }
    SidebarComponent.prototype.isNotMobileMenu = function () {
        if (window.outerWidth > 991) {
            return false;
        }
        return true;
    };
    SidebarComponent.prototype.ngOnInit = function () {
        this.menuItems = exports.ROUTES.filter(function (menuItem) { return menuItem; });
    };
    SidebarComponent.prototype.ngAfterViewInit = function () {
    };
    SidebarComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-sidebar',
            templateUrl: 'sidebar.component.html',
        })
    ], SidebarComponent);
    return SidebarComponent;
}());
exports.SidebarComponent = SidebarComponent;
//# sourceMappingURL=sidebar.component.js.map