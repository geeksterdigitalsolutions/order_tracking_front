"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var user_component_1 = require("./user.component");
exports.UserRoutes = [{
        path: '',
        children: [{
                path: 'pages/user',
                component: user_component_1.UserComponent
            }]
    }];
//# sourceMappingURL=user.routing.js.map