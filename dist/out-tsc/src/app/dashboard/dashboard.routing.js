"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var dashboard_component_1 = require("./dashboard.component");
exports.DashboardRoutes = [{
        path: '',
        children: [{
                path: 'dashboard',
                component: dashboard_component_1.DashboardComponent
            }]
    }];
//# sourceMappingURL=dashboard.routing.js.map