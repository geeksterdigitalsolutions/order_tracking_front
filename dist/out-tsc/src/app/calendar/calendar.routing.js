"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var calendar_component_1 = require("./calendar.component");
exports.CalendarRoutes = [{
        path: '',
        children: [{
                path: '',
                component: calendar_component_1.CalendarComponent
            }]
    }];
//# sourceMappingURL=calendar.routing.js.map