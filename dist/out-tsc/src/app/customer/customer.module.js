"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var sidebar_module_1 = require("../sidebar/sidebar.module");
var footer_module_1 = require("../shared/footer/footer.module");
var navbar_module_1 = require("../shared/navbar/navbar.module");
var customer_routing_1 = require("./customer.routing");
var customer_component_1 = require("./customer.component");
var dashboard_component_1 = require("./dashboard/dashboard.component");
var booking_component_1 = require("./booking/booking.component");
var car_registration_component_1 = require("./car-registration/car-registration.component");
var core_2 = require("@agm/core"); // @agm/core
var agm_direction_1 = require("agm-direction"); // agm-direction
var detail_component_1 = require("./car-registration/detail/detail.component");
var CustomerModule = /** @class */ (function () {
    function CustomerModule() {
    }
    CustomerModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                router_1.RouterModule.forChild(customer_routing_1.CustomerRoutes),
                forms_1.FormsModule,
                sidebar_module_1.SidebarModule,
                navbar_module_1.NavbarModule,
                footer_module_1.FooterModule,
                core_2.AgmCoreModule.forRoot({ apiKey: 'AIzaSyChFUZCAJqpD6QxEBK3dGNzAVpYJsn5N9o' }),
                agm_direction_1.AgmDirectionModule,
            ],
            declarations: [
                customer_component_1.CustomerComponent,
                dashboard_component_1.DashboardComponent,
                booking_component_1.BookingComponent,
                car_registration_component_1.CarRegistrationComponent,
                detail_component_1.CarRegistrationDetailComponent
            ],
            schemas: [core_1.CUSTOM_ELEMENTS_SCHEMA]
        })
    ], CustomerModule);
    return CustomerModule;
}());
exports.CustomerModule = CustomerModule;
//# sourceMappingURL=customer.module.js.map