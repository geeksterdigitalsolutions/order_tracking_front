"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var chart_js_1 = require("chart.js");
var DashboardComponent = /** @class */ (function () {
    function DashboardComponent(router) {
        this.router = router;
        this.bookingData = {
            headerRow: ['BOOKING NO', 'INVOICE', 'VOLUME', 'DESTINATION', 'PAYMENT STATUS', 'SHIPMENT STATUS', 'DOCUMENT STATUS', 'ETD', 'ETA'],
            dataRows: []
        };
    }
    DashboardComponent.prototype.ngOnInit = function () {
        this.bookingData.dataRows = [
            ['90800247871', 'PNP1SHM18110002', '10', 'OSAKA, JAPAN', 'รับเงินโอนแล้ว', 'PORT OF LOADING', 'COMPLETED', '05/11/2018', '20/11/2018'],
            ['SU-13024', 'PNP1SHM18110003', '8', 'OSAKA, JAPAN', 'รับเงินโอนแล้ว', 'PORT OF LOADING', 'COMPLETED', '27/11/2018', '30/11/2018'],
            ['SU-13025', 'PNP1SHM18110004', '12', 'OSAKA, JAPAN', 'รับเงินโอนแล้ว', 'MALAYSIA SIDE', 'ON PROCESS', '30/11/2018', '03/12/2018'],
            ['SU-13026', 'PNP1SHM18110005', '7', 'OSAKA, JAPAN', 'รอโอนเงิน', 'PNP FACTORY', 'ON PROCESS', '14/12/2018', '16/12/2018']
        ];
        this.canvas = document.getElementById('chartShipment');
        this.ctx = this.canvas.getContext('2d');
        this.myChart = new chart_js_1.default(this.ctx, {
            type: 'pie',
            data: {
                labels: ['PROCESSING', 'COMPLETED'],
                datasets: [{
                        label: 'Shipment',
                        pointRadius: 0,
                        pointHoverRadius: 0,
                        backgroundColor: [
                            '#fcc468',
                            '#4acccd'
                        ],
                        borderWidth: 0,
                        data: [4, 0]
                    }]
            },
            options: {
                legend: {
                    display: true
                },
                tooltips: {
                    enabled: true
                },
                scales: {
                    yAxes: [{
                            ticks: {
                                display: false
                            },
                            gridLines: {
                                drawBorder: false,
                                zeroLineColor: 'transparent',
                                color: 'rgba(255,255,255,0.05)'
                            }
                        }],
                    xAxes: [{
                            barPercentage: 1.6,
                            gridLines: {
                                drawBorder: false,
                                color: 'rgba(255,255,255,0.1)',
                                zeroLineColor: 'transparent'
                            },
                            ticks: {
                                display: false,
                            }
                        }]
                },
            }
        });
        this.canvas = document.getElementById('chartDocument');
        this.ctx = this.canvas.getContext('2d');
        this.myChart = new chart_js_1.default(this.ctx, {
            type: 'pie',
            data: {
                labels: ['PROCESSING', 'COMPLETED'],
                datasets: [{
                        label: 'Document',
                        pointRadius: 0,
                        pointHoverRadius: 0,
                        backgroundColor: [
                            '#fcc468',
                            '#4acccd'
                        ],
                        borderWidth: 0,
                        data: [2, 2]
                    }]
            },
            options: {
                legend: {
                    display: true
                },
                tooltips: {
                    enabled: true
                },
                scales: {
                    yAxes: [{
                            ticks: {
                                display: false
                            },
                            gridLines: {
                                drawBorder: false,
                                zeroLineColor: 'transparent',
                                color: 'rgba(255,255,255,0.05)'
                            }
                        }],
                    xAxes: [{
                            barPercentage: 1.6,
                            gridLines: {
                                drawBorder: false,
                                color: 'rgba(255,255,255,0.1)',
                                zeroLineColor: 'transparent'
                            },
                            ticks: {
                                display: false,
                            }
                        }]
                },
            }
        });
    };
    DashboardComponent.prototype.ngAfterViewInit = function () {
        $('#datatable').DataTable({
            'pagingType': 'full_numbers',
            'lengthMenu': [
                [10, 25, 50, -1],
                [10, 25, 50, 'All']
            ],
            responsive: true,
            language: {
                search: '_INPUT_',
                searchPlaceholder: 'Searching',
            }
        });
        var table = $('#datatable').DataTable();
    };
    DashboardComponent.prototype.linkTo = function (row) {
        this.router.navigate(['/c/booking']);
    };
    DashboardComponent = __decorate([
        core_1.Component({
            selector: 'app-dashboard',
            templateUrl: './dashboard.component.html'
        }),
        __metadata("design:paramtypes", [router_1.Router])
    ], DashboardComponent);
    return DashboardComponent;
}());
exports.DashboardComponent = DashboardComponent;
//# sourceMappingURL=dashboard.component.js.map