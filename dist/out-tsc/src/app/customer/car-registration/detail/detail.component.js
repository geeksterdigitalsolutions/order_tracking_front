"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var CarRegistrationDetailComponent = /** @class */ (function () {
    function CarRegistrationDetailComponent() {
        this.lat = 24.799448;
        this.lng = 120.979021;
    }
    CarRegistrationDetailComponent.prototype.ngOnInit = function () {
        this.getDirection();
    };
    CarRegistrationDetailComponent.prototype.ngAfterViewInit = function () {
    };
    CarRegistrationDetailComponent.prototype.getDirection = function () {
        this.origin = { lat: 24.799448, lng: 120.979021 };
        this.destination = { lat: 24.799524, lng: 120.975017 };
        // this.origin = 'Taipei Main Station';
        // this.destination = 'Taiwan Presidential Office';
    };
    CarRegistrationDetailComponent = __decorate([
        core_1.Component({
            selector: 'app-car-registration-detail',
            templateUrl: './detail.component.html',
            styleUrls: ['./detail.component.css']
        })
    ], CarRegistrationDetailComponent);
    return CarRegistrationDetailComponent;
}());
exports.CarRegistrationDetailComponent = CarRegistrationDetailComponent;
//# sourceMappingURL=detail.component.js.map