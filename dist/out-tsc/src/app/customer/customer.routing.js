"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var dashboard_component_1 = require("./dashboard/dashboard.component");
var booking_component_1 = require("./booking/booking.component");
var car_registration_component_1 = require("./car-registration/car-registration.component");
var detail_component_1 = require("./car-registration/detail/detail.component");
exports.CustomerRoutes = [{
        path: '',
        children: [
            {
                path: 'dashboard',
                component: dashboard_component_1.DashboardComponent
            },
            {
                path: 'booking',
                component: booking_component_1.BookingComponent
            },
            {
                path: 'car-registration',
                component: car_registration_component_1.CarRegistrationComponent
            },
            {
                path: 'car-registration/70-5530',
                component: detail_component_1.CarRegistrationDetailComponent
            }
        ]
    }];
//# sourceMappingURL=customer.routing.js.map