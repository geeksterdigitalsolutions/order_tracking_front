"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var widgets_component_1 = require("./widgets.component");
exports.WidgetsRoutes = [{
        path: '',
        children: [{
                path: 'widgets',
                component: widgets_component_1.WidgetsComponent
            }]
    }];
//# sourceMappingURL=widgets.routing.js.map