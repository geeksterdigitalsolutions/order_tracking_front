"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var timeline_component_1 = require("./timeline.component");
exports.TimelineRoutes = [{
        path: '',
        children: [{
                path: 'pages/timeline',
                component: timeline_component_1.TimelineComponent
            }]
    }];
//# sourceMappingURL=timeline.routing.js.map