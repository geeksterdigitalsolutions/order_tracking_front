import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SidebarModule } from '../sidebar/sidebar.module';
import { FooterModule } from '../shared/footer/footer.module';
import { NavbarModule} from '../shared/navbar/navbar.module';

import { CustomerRoutes } from './customer.routing';
import { CustomerComponent } from './customer.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BookingComponent } from './booking/booking.component';
import { CarRegistrationComponent } from './car-registration/car-registration.component';
import { AgmCoreModule } from '@agm/core'; // @agm/core
import { AgmDirectionModule } from 'agm-direction';   // agm-direction
import { CarRegistrationDetailComponent } from './car-registration/detail/detail.component';

@NgModule({
    imports: [
      CommonModule,
      RouterModule.forChild(CustomerRoutes),
      FormsModule,
      SidebarModule,
      NavbarModule,
      FooterModule,
      AgmCoreModule.forRoot({ apiKey: 'AIzaSyChFUZCAJqpD6QxEBK3dGNzAVpYJsn5N9o' }),
      AgmDirectionModule,     // agm-direction
    ],
    declarations: [
      CustomerComponent,
      DashboardComponent,
      BookingComponent,
      CarRegistrationComponent,
      CarRegistrationDetailComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class CustomerModule {}
