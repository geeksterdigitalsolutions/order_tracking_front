import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router'
import Chart from 'chart.js';

declare const $: any;

@Component({
  selector: 'app-car-registration',
  templateUrl: './car-registration.component.html'
})
export class CarRegistrationComponent implements OnInit, AfterViewInit {

  bookingData = {
    headerRow: ['Car Registration', 'INVOICE', 'VOLUME', 'DESTINATION', 'PAYMENT STATUS', 'SHIPMENT STATUS', 'DOCUMENT STATUS', 'ETD', 'ETA'],
    dataRows: []
  }

  public canvas: any;
  public ctx;
  public myChart: any;

  constructor(private router: Router) {}

  public ngOnInit() {
    this.bookingData.dataRows = [
      ['70-5530', 'PNP1SHM18110002', '10', 'OSAKA, JAPAN', 'รับเงินโอนแล้ว', 'PORT OF LOADING', 'COMPLETED', '05/11/2018', '20/11/2018'],
    ]

    this.canvas = document.getElementById('chartShipment');
    this.ctx = this.canvas.getContext('2d');

    this.myChart = new Chart(this.ctx, {
      type: 'pie',
      data: {
        labels: ['PROCESSING', 'COMPLETED'],
        datasets: [{
          label: 'Shipment',
          pointRadius: 0,
          pointHoverRadius: 0,
          backgroundColor: [
            '#fcc468',
            '#4acccd'
          ],
          borderWidth: 0,
          data: [4, 0]
        }]
      },
      options: {
        legend: {
          display: true
        },
        tooltips: {
          enabled: true
        },

        scales: {
          yAxes: [{
            ticks: {
              display: false
            },
            gridLines: {
              drawBorder: false,
              zeroLineColor: 'transparent',
              color: 'rgba(255,255,255,0.05)'
            }

          }],

          xAxes: [{
            barPercentage: 1.6,
            gridLines: {
              drawBorder: false,
              color: 'rgba(255,255,255,0.1)',
              zeroLineColor: 'transparent'
            },
            ticks: {
              display: false,
            }
          }]
        },
      }
    });

    this.canvas = document.getElementById('chartDocument');
    this.ctx = this.canvas.getContext('2d');

    this.myChart = new Chart(this.ctx, {
      type: 'pie',
      data: {
        labels: ['PROCESSING', 'COMPLETED'],
        datasets: [{
          label: 'Document',
          pointRadius: 0,
          pointHoverRadius: 0,
          backgroundColor: [
            '#fcc468',
            '#4acccd'
          ],
          borderWidth: 0,
          data: [2, 2]
        }]
      },
      options: {
        legend: {
          display: true
        },
        tooltips: {
          enabled: true
        },

        scales: {
          yAxes: [{
            ticks: {
              display: false
            },
            gridLines: {
              drawBorder: false,
              zeroLineColor: 'transparent',
              color: 'rgba(255,255,255,0.05)'
            }

          }],

          xAxes: [{
            barPercentage: 1.6,
            gridLines: {
              drawBorder: false,
              color: 'rgba(255,255,255,0.1)',
              zeroLineColor: 'transparent'
            },
            ticks: {
              display: false,
            }
          }]
        },
      }
    });
  }

  ngAfterViewInit() {
    $('#datatable').DataTable({
      'pagingType': 'full_numbers',
      'lengthMenu': [
        [10, 25, 50, -1],
        [10, 25, 50, 'All']
      ],
      responsive: true,
      language: {
        search: '_INPUT_',
        searchPlaceholder: 'Searching',
      }
    });

    const table = $('#datatable').DataTable();
  }

  linkTo(row) {
    this.router.navigate(['/c/car-registration/70-5530'])
  }
}
