import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarRegistrationDetailComponent } from './detail.component';

describe('CarRegistrationDetailComponent', () => {
  let component: CarRegistrationDetailComponent;
  let fixture: ComponentFixture<CarRegistrationDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarRegistrationDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarRegistrationDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
