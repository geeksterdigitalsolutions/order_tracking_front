import { Component, OnInit, AfterViewInit } from '@angular/core';
import Chart from 'chart.js';

declare const $: any;

@Component({
  selector: 'app-car-registration-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class CarRegistrationDetailComponent implements OnInit, AfterViewInit {

  public lat = 24.799448;
  public lng = 120.979021;
 
  public origin: any;
  public destination: any;
 
  ngOnInit() {
    this.getDirection();
  }

  public ngAfterViewInit() {

  }
 
  getDirection() {
    this.origin = { lat: 24.799448, lng: 120.979021 };
    this.destination = { lat: 24.799524, lng: 120.975017 };
  
    // this.origin = 'Taipei Main Station';
    // this.destination = 'Taiwan Presidential Office';
  }
}
