import { Routes } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { BookingComponent } from './booking/booking.component';
import { CarRegistrationComponent } from './car-registration/car-registration.component';
import { CarRegistrationDetailComponent } from './car-registration/detail/detail.component';

export const CustomerRoutes: Routes = [{
    path: '',
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'booking',
        component: BookingComponent
      },
      {
        path: 'car-registration',
        component: CarRegistrationComponent
      },
      {
        path: 'car-registration/70-5530',
        component: CarRegistrationDetailComponent
      }
    ]
}];
